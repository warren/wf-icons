module.exports = function(grunt) {

	grunt.initConfig({
		webfont: {
			icons: {
				src: 'svg/*.svg',
				dest: 'build/fonts',
				options: {
					engine: 'node',
					font: 'wf-icons',
					startCodepoint: 0xF101,
					codepoints: {
						invoice: 0x2000
					}
				}
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-webfont');

	grunt.registerTask('default', ['webfont']);
}

